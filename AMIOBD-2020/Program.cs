﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMIOBD_2020
{
    class Program
    {
        static void Main()
        {
            // Generate Connection String
            OdbcConnectionStringBuilder constr = new OdbcConnectionStringBuilder
            {
                { "DSN", "amiobd" },  // can be initialized as constr.Dsn = "amiodb";
                { "Database", "data" }
            };

            // Connect to Database using ODBC DSN
            OdbcConnection con = new OdbcConnection(constr.ConnectionString);

            // Open Connection to Cassandra
            con.Open();

            // Create SQL statement
            string SQL = "select count(*) from users where login=@login and pwd=@pwd;";

            OdbcCommand cmd = new OdbcCommand
            {
                Connection = con,
                CommandText = SQL
            };

            // Init and Send Parameter to ODBC Command
            // Parameter can be sended as ? or @name (but the second one always does not work with free drivers)
            OdbcParameter param = new OdbcParameter
            {
                ParameterName = "@login",
                DbType = DbType.String,
                OdbcType = OdbcType.Text,
                Value = "ivanov"
            };
            cmd.Parameters.Add(param); // add initialized parameter

            // Init and Send Parameter to ODBC Command
            param = new OdbcParameter
            {
                ParameterName = "@pwd",
                DbType = DbType.String,
                OdbcType = OdbcType.Text,
                Value = "Qwerty789"
            };
            cmd.Parameters.Add(param); // add initialized parameter

            // Get result from Cassandra NoSQL DBMS
            int result = Convert.ToInt32(cmd.ExecuteScalar());

            if (result == 1)
            {
                Console.WriteLine("Auth OK");
            }
            else
            {
                Console.WriteLine("Auth Fail");
            }

            // Add multiple line result

            cmd.CommandText = "SELECT * FROM users";
            OdbcDataReader dbr = cmd.ExecuteReader();

            
            while (dbr.Read())
            {
                Console.WriteLine("User {0} has {1} artefact(s)",dbr.GetString(4), dbr.GetValue(3));
            }
                // Dispose cmd Object
                cmd.Dispose();

            // =========== Additional Info ===========
            // Create Command -> execute SQL Statement 
            // 1. multiple lines in result (select * from...) command.ExecuteQuery
            // 2. single line in result ( COUNT, MAX, MIN, MEAN in SQL, ex. select count(*) from... ) command.ExecuteScalar
            // 3. no output in result (INSERT, UPDATE, DELETE, TRUNCATE,...) command.ExecuteNonQuery
            // Parameters -> for where a ='z' or b = 'x', transfer parameter from code, for preventing SQL injection

            // Close Connection to Cassandra
            con.Close();
        }
    }
}
